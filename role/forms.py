from django import forms
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django.contrib.auth.models import User,Group
from django.utils.translation import gettext_lazy as _

from .models import Country,UserExtraDetails


class RegisterForm(UserCreationForm):
    field_order = ["username", "email", "role", "country", "nationality","mobile","password1", "password2"]

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.fields['role'] = forms.ModelChoiceField(queryset=self.get_role_choices())
        self.fields['mobile'] = forms.CharField(max_length=255)
        self.fields['country'] = forms.ModelChoiceField(queryset=self.get_country_choices())
        self.fields['nationality'] = forms.CharField(max_length=255)
        self.fields['email'].required = True
        self.order_fields(self.field_order)
        
    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()

        self.cleaned_data['role'].user_set.add(user)


        userdetails = UserExtraDetails()
        userdetails.user = user
        userdetails.country = self.cleaned_data['country']
        userdetails.nationality = self.cleaned_data['nationality']
        userdetails.mobile = self.cleaned_data['mobile']
        userdetails.save()
        return user

    def get_role_choices(self):
        return Group.objects.all()

    def get_country_choices(self):
        return Country.objects.all()

class EmailLoginForm(AuthenticationForm):
    def __init__(self, request=None, *args, **kwargs):
        super().__init__(request,*args, **kwargs)
        self.fields['username'] = forms.EmailField(label='Email')
        self.error_messages.update({
            'invalid_login': _(
                "Please enter a correct Email and password. Note that both "
                "fields may be case-sensitive."
            )
        })

    def get_invalid_login_error(self):
        return forms.ValidationError(
            self.error_messages['invalid_login'],
            code='invalid_login',
            params={},
        )