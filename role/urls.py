from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path('',views.HomeView.as_view(),name='home'),
    path('register/',views.RegisterView.as_view(),name='register'),
    path('login/', views.LoginView.as_view(),name='login'),
    path('student/profile/', views.StudentProfileView.as_view(),name='student_profile'),
    path('staff/profile/', views.StaffProfileView.as_view(),name='staff_profile'),
    path('editor/profile/', views.EditorProfileView.as_view(),name='editor_profile'),
    path('admin/profile/', views.AdminProfileView.as_view(),name='admin_profile'),
]