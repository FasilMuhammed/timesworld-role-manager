from django.contrib import admin

from .models import Country,UserExtraDetails

admin.site.register(Country)
admin.site.register(UserExtraDetails)