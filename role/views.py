from django.urls import reverse
from django.shortcuts import redirect, render
from django.views import View
from django.contrib.auth.views import LoginView as AuthLoginView
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required,permission_required
from django.utils.decorators import method_decorator

from .forms import RegisterForm,EmailLoginForm

class HomeView(View):
    def get(self,request,*args,**kwargs):
        context = {}
        context['title'] = 'Home'
        return render(request,'home.html',context)

class RegisterView(View):
    form_class = RegisterForm
    template_name = 'registration/register.html'

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return HttpResponseRedirect(reverse('home'))
        return super().dispatch(request, *args, **kwargs)

    def get(self,request,*args,**kwargs):
        form = self.form_class()
        return self.render(form)

    def post(self,request,*args,**kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('login'))
        else:
            return self.render(form)
    
    def render(self,form):
        context = self.get_context_data()
        context['form'] = form
        context['form'].buttons = self.get_form_buttons()
        return render(self.request,self.template_name,context)
        
    def get_form_buttons(self):
        return [
            {'label' : 'Register','class':'btn btn-primary'}
        ]
    
    def get_context_data(self):
        context = {}
        context['title'] = 'Register'
        return context
        
class LoginView(AuthLoginView):
    redirect_authenticated_user=True
    extra_context = {
        'title' : 'Login'
    }
    form_class = EmailLoginForm
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'].buttons = self.get_form_buttons()
        return context
    
    def get_form_buttons(self):
        return [
            {'label' : 'Login','class':'btn btn-primary'}
        ]

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('role.student_permissions',raise_exception=True), name='dispatch')
class StudentProfileView(View):
    template_name = 'role/student.html'
    def get(self,request,*args,**kwargs):
        context = {}
        context['title'] = 'Student'
        return render(request,self.template_name,context)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('role.staff_permissions',raise_exception=True), name='dispatch')
class StaffProfileView(View):
    template_name = 'role/staff.html'
    def get(self,request,*args,**kwargs):
        context = {}
        context['title'] = 'Staff'
        return render(request,self.template_name,context)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('role.editor_permissions',raise_exception=True), name='dispatch')
class EditorProfileView(View):
    template_name = 'role/editor.html'
    def get(self,request,*args,**kwargs):
        context = {}
        context['title'] = 'Editor'
        return render(request,self.template_name,context)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('role.admin_permissions',raise_exception=True), name='dispatch')
class AdminProfileView(View):
    template_name = 'role/admin.html'
    def get(self,request,*args,**kwargs):
        context = {}
        context['title'] = 'Admin'
        return render(request,self.template_name,context)