from django.db import models
from django.contrib.auth.models import User

class Country(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=10)

    def __str__(self):
        return self.name

class UserExtraDetails(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    country = models.ForeignKey(Country,on_delete=models.CASCADE)
    nationality = models.CharField(max_length=255)
    mobile = models.CharField(max_length=255)

    class Meta:
        permissions = (
            ('student_permissions',"Student Permissions"),
            ('staff_permissions',"Staff Permissions"),
            ('admin_permissions',"Admin Permissions"),
            ('editor_permissions',"Editor Permissions"),
        )

    def __str__(self):
        return self.user.email

