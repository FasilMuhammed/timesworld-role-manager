Installation:

step 1 : create a virtual environment by using "venv" or "pipenv".


step 2 : install the packages - run "pip install -r requirements.txt"


step 3 : run the application - python3 manage.py runserver


Working :


User registers with his details and login with his emails. Based on the role ( student,staff,admin,editor) he will be redirected to the respective home page.


for eg,


if the user has the role "student" he will have "home", "student profile" ,"logout" menus in his home page. He will be redirected to a "403 permission denied" page if he try to access any other role's pages.



Note : 

For the coutries, i have created a model to save the data. Currently i have added "India", "United States" and "United Kingdom". If we need to have more countries, we can add it by log in into the admin dashboard 


    
    email : "superadmin@testemail.com",

    password : "As@12345"
 


Database Used : Sqlite3
